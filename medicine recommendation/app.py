from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
import os
import numpy as np
from flaskext.mysql import MySQL
import pandas as pd
import joblib

app = Flask(__name__)


basedir = os.path.abspath(os.path.dirname(__file__))
# print(basedir)

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:@localhost/research'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
ma = Marshmallow(app)
 
# User Table Model 
class final_dataset4(db.Model):
  id=db.Column(db.Integer, primary_key=True)
  drugName = db.Column(db.String(500))
  conditionName = db.Column(db.String(500))
  rating = db.Column(db.String(100))
  dosage = db.Column(db.String(500))
  sideEffects = db.Column(db.String(500))
  price = db.Column(db.String(500))
  precaution1 = db.Column(db.String(500))
  precaution2 = db.Column(db.String(500))
  precaution3 = db.Column(db.String(500))
  precaution4 = db.Column(db.String(500))
  description = db.Column(db.String(500))
  recommended = db.Column(db.String(500))
 
  def __init__(self,drugName,conditionName,rating, dosage, sideEffects, price, precaution1, precaution2, precaution3, precaution4, description, recommended) :
    self.drugName=drugName
    self.conditionName=conditionName
    self.rating=rating
    self.dosage=dosage
    self.sideEffects=sideEffects
    self.price=price
    self.precaution1=precaution1
    self.precaution2=precaution2
    self.precaution3=precaution3
    self.precaution4=precaution4
    self.description=description
    self.recommended=recommended

 
class MedSchema(ma.Schema):
  class Meta:
    fields = ('id','drugName','conditionName','rating', 'dosage', 'sideEffects', 'price', 'precaution1', 'precaution2', 'precaution3', 'precaution4', 'description', 'recommended')
 
med_schema = MedSchema()
meds_schema=MedSchema(many=True)
 
# Add New User
@app.route('/medstore',methods=['POST'])
def add_record():
  drugName=request.json['drugName']
  conditionName=request.json['conditionName']
  rating=request.json['rating']
  dosage=request.json['dosage']
  sideEffects=request.json['sideEffects']
  price=request.json['price']
  precaution1=request.json['precaution1']
  precaution2=request.json['precaution2']
  precaution3=request.json['precaution3']
  precaution4=request.json['precaution4']
  description=request.json['description']
  recommended=request.json['recommended']

  new_record=final_dataset4(drugName,conditionName,rating, dosage, sideEffects, price, precaution1, precaution2, precaution3, precaution4, description, recommended )
  db.session.add(new_record)
  db.session.commit()
  return med_schema.jsonify(new_record)
 
# Show All records
@app.route('/medstore',methods=['GET'])
def getAllrecords():
  all_records=final_dataset4.query.all()
  result=meds_schema.dump(all_records)
  return jsonify(result)


# Show record By ID
@app.route('/medstore/<id>',methods=['GET'])
def getRecordByid(id):
  record=final_dataset4.query.get(id)
  return med_schema.jsonify(record)


#filtering by condition
@app.route('/condfilter/<conditionName>',methods=['GET'])
def filterbycondition(conditionName):
  filterdata= final_dataset4.query.filter(final_dataset4.conditionName==conditionName )
  results = filterdata.all()
  return meds_schema.jsonify(results)

#filtering by drugname
@app.route('/drugfilter/<drugName>',methods=['GET'])
def filterbydrug(drugName):
  filterdatabydrug= final_dataset4.query.filter(final_dataset4.drugName==drugName )
  results = filterdatabydrug.all()
  return meds_schema.jsonify(results)


# Update User By ID
@app.route('/medstore/<id>',methods=['PUT'])
def insertNewRating(id):
  record=final_dataset4.query.get(id)
  drugName=request.json['drugName']
  conditionName=request.json['conditionName']
  rating=request.json['rating']
  dosage=request.json['dosage']
  sideEffects=request.json['sideEffects']
  price=request.json['price']
  precaution1=request.json['precaution1']
  precaution2=request.json['precaution2']
  precaution3=request.json['precaution3']
  precaution4=request.json['precaution4']
  description=request.json['description']
  recommended=request.json['recommended']
  
  record.drugName=drugName
  record.conditionName=conditionName
  record.rating=rating
  record.dosage=dosage
  record.sideEffects=sideEffects
  record.price=price
  record.precaution1=precaution1
  record.precaution2=precaution2
  record.precaution3=precaution3
  record.precaution4=precaution4
  record.description=description
  record.recommended=recommended


  db.session.commit()
  return med_schema.jsonify(record)
 

#Load the pickle model
model = joblib.load("modelGNB.pkl")


@app.route("/recommand", methods=["POST"])
def predict1():
    int_features =[]
    json_ = request.get_json()
    print(json_)
    for key, value in json_[0].items():
        #print(key, value)
        int_features.append(float(value))
    features = [np.array(int_features)]
    print("features",features)
    # prediction = model.predict(final_features)
    prediction = model.predict(features)
    print(prediction)

    if prediction == 1:
        return jsonify({"prediction_text":'This Medicine can be Recommended'})
    else:
        return jsonify({"prediction_text":'This Medicine can not be Recommended'})
 
if __name__ == '__main__':
    app.run(debug=True, port=7500)