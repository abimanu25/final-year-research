import 'package:cxr_scan/screen/app_drawer.dart';
import 'package:cxr_scan/screen/title_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
// import 'package:flutter_unity_widget/flutter_unity_widget.dart';

class ARScreen extends StatelessWidget {
  const ARScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Color.fromARGB(255, 143, 139, 139),
        title: const TitleBar(
          sectionName: 'AR for Lung Anatomy',
        ),
      ),
      body: Card(
        margin: const EdgeInsets.all(10),
        clipBehavior: Clip.antiAlias,
        child: Stack(
          children: <Widget>[
            // UnityWidget(
            //   onUnityCreated: onUnityCreated,
            //   )
          ],
        ),
      ),
    );
  }
  //  void onUnityCreated(UnityWidgetController controller) {
  // }
}
