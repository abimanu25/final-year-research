import mysql.connector
import json

def create_con():
    MyDB = mysql.connector.connect(
    host="cxr-database.czbmndal6xg0.us-east-1.rds.amazonaws.com",
    user="cxr_user",
    password="cxr_user",
    database="cxr_scan"
    )
    return MyDB

def insert(sql_query, args):
    db = create_con()
    cursor = db.cursor()
    try:
        cursor.execute(sql_query, args)
        db.commit()
        db.close()
        return args
    except Exception as e:
        db.rollback()
        db.close()
        print(e)
        return e
