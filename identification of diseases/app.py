#http://buffml.com/

from flask import Flask, render_template, request, jsonify
from keras.models import load_model
from keras.preprocessing import image
from keras.utils import image_utils
import numpy as np 
import cv2
import os
import io
from werkzeug.utils import secure_filename
from config.dbConfig import *

app = Flask(__name__)

#Image Size
img_size=256
model = load_model('model/model_vgg16_2.h5')
model1 = load_model('model/validate_model_vgg16_2.h5')



model.make_predict_function()

  

def predict_label(img_path):
    # load the image, swap color channels, and resize it to be a fixed
    # 224x224 pixels while ignoring aspect ratio
    image = cv2.imread(img_path)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    image = cv2.resize(image, (224, 224))
    # update the data and labels lists, respectively
    #data.append(image)
    #labels.append(label)
    img = np.array(image) / 255.0
    #labels = np.array(labels) 
    img = np.array([img])
    img.shape
    predict_v=model1.predict(img) 
    classes_v=np.argmax(predict_v,axis=1)
    if classes_v[0]== 2:
        validation = 'Valid'

        predict_x=model.predict(img) 
        classes_x=np.argmax(predict_x,axis=1)
        if classes_x[0]== 0:
            result = 'Lung Cancer'
        elif classes_x[0]== 1:
            result = 'Healthy'
        else: 
            result = 'Tuberculosis'
    else:
        result = 'Invalid Image Please Upload Chest X ray image'

    return result   


# routes
@app.route("/", methods=['GET', 'POST'])
def main():
    return render_template("index.html")



@app.route("/predict", methods = ['GET', 'POST'])
def upload():
    
    if request.method == 'POST':
        try:
            
            img = request.files['file']
            img_path = "assets/" + img.filename  
            print(img_path)  
            img.save(img_path)
            p = predict_label(img_path)
            print("server started")
            print(p)
            #return str(p)
       

            # get request parameters
            # file = request.files['file']
            # filename = secure_filename(file.filename)
            # filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            # file.save(filepath)

            # disease = request.form['disease']

            MyDB = create_con()
            myCursor = MyDB.cursor()

            # myCursor.execute("CREATE TABLE IF NOT EXISTS disease_images (id INTEGER(45) NOT NULL AUTO_INCREMENT PRIMARY KEY,image LONGBLOB NOT NULL")
            myCursor.execute(
                "CREATE TABLE IF NOT EXISTS disease_images (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,image LONGBLOB NOT NULL,disease VARCHAR(255))")
            with open(img_path, 'rb') as File:
                BinaryData = File.read()

            selectSQLQry = "SELECT * FROM disease_images"
            myCursor.execute(selectSQLQry)
            myResult = myCursor.fetchall()

            if(len(myResult)>0):
                updateSQLQry = "UPDATE disease_images SET image =  %s, disease =%s WHERE id = 1"
                val = (BinaryData, p)
                myCursor.execute(updateSQLQry, val)
                MyDB.commit()
            else:
                    insertSQLQry = "INSERT INTO disease_images (image,disease) VALUES (%s, %s)"
                    val = (BinaryData, p)
                    myCursor.execute(insertSQLQry, val)
                    MyDB.commit()

            myCursor.close()
            return str(p)

        except Exception as e:
            jsonify({
                "message": str(e),
                "code": 500,
            })

if __name__ =='__main__':
    app.run( port = 6000 ,debug = True)